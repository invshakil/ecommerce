<html>
    <head>
        <title>Home</title>
    </head>
    <body>
        <h3>Order Date: 21.04.2016</h3>
        <table border='1' style="width: 80%; border: 1px solid red; ">
            <tr style="text-align: center">
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Product Price</th>
                <th>Product Quantity</th>
                <th>Total Price</th>
            </tr>
            <tr>
                <td>2</td>
                <td>Sharukh Khan</td>
                <td>BDT: 1200</td>
                <td>3</td>
                <td>BDT: 3600</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Salman Khan</td>
                <td>BDT: 1600</td>
                <td>3</td>
                <td>BDT: 4800</td>
            </tr>
        </table>
    </body>
</html>