<?php
    $product_id=$_GET['product_id'];
    $query_result=$ob_app->select_product_info_by_id($product_id);
    
    $product_info=mysqli_fetch_assoc($query_result);
//        echo '<pre>';
//    print_r($product_info);
    
    $category_id=$product_info['category_id'];
    $category_query_result=$ob_app->select_product_by_category_id($category_id);
    
    if(isset($_POST['cart_btn'])) {
        
        $ob_app->save_cart_product_info($_POST);
    }
    

?>

<div class="men">
    <div class="container">
        <div class="single_top">
            <div class="col-md-9 single_right">
                <div class="grid images_3_of_2">
                    <ul id="etalage">
                        
					<li>
                            <img class="etalage_thumb_image" src="admin/<?php echo $product_info['product_image']; ?>" class="img-responsive" />
                            
                        </li>
					
					</ul>
                    <div class="clearfix"></div>		
                </div> 
                <div class="desc1 span_3_of_2">
                    <h1><u>Doctor  Name:</u> <?php echo $product_info['product_name']; ?></h1>
                    <p><u>Product Price:</u> BDT <?php echo $product_info['product_price']; ?></p>
                    <p><u>Category Name:</u> <?php echo $product_info['category_name']; ?></p>
                    <p><u>Area Name:</u> <?php echo $product_info['manufacture_name']; ?></p>
                    <p><u>Visiting Hours:</u> <?php echo $product_info['product_quantity']; ?></p>
                    <div class="btn_form">
                        <form action="" method="post">
                            <input type="hidden" name="product_quantity" value="1"> 
                            <input type="hidden" name="product_id" value="<?php echo $product_info['product_id']; ?>"> 
                            <input type="submit" name="cart_btn" value="View Details">
                        </form>
                    </div>
                    
                </div>
                <div class="clearfix"></div>	
            </div>
            <div class="col-md-3">
                <!-- FlexSlider -->
                <section class="slider_flex">
                    <div class="flexslider">
                        <ul class="slides">
                            <li><img src="images/pic4.jpg" class="img-responsive" alt=""/></li>
                            <li><img src="images/pic7.jpg" class="img-responsive" alt=""/></li>
                            <li><img src="images/pic6.jpg" class="img-responsive" alt=""/></li>
                            <li><img src="images/pic5.jpg" class="img-responsive" alt=""/></li>
                        </ul>
                    </div>
                </section>
                <!-- FlexSlider -->
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="toogle">
            <h2>Doctor's Short Description</h2>
            <p class="m_text2"><?php echo $product_info['product_short_description']; ?></p>
        </div>
        <div class="toogle">
            <h2>Doctor's Long Description</h2>
            <p class="m_text2"><?php echo $product_info['product_long_description']; ?></p>
        </div>
        <h4 class="head_single">Related Products</h4>
        <div class="span_3">
            <?php while ($category_product=  mysqli_fetch_assoc($category_query_result)) { ?>
            <div class="col-sm-3 grid_1">
                <a href="single.html">
                    <img src="admin/<?php echo $category_product['product_image']; ?>" class="img-responsive" alt=""/>
                    <h3><?php echo $category_product['product_name']; ?></h3>
                    <hr/>
                    <h4>BDT <?php echo $category_product['product_price']; ?></h4>
                </a>  
            </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
    
    
    
    
    
    
    
</div>  