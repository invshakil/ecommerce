<?php
    $query_result=$ob_app->select_all_published_product();
    
    
?>
<div class="index_slider">
    <div class="container">
        <div class="callbacks_container">
            <ul class="rslides" id="slider">
                
            </ul>
        </div>
    </div> 
</div>
<div class="content_top">
    <div class="container">
        <div class="grid_1">
            <div class="col-md-3">
                <div class="box2">
                    <ul class="list1">
                        <i class="lock"> </i>
                        <li class="list1_right"><p>Upto 5% Reward on your shipping</p></li>
                        <div class="clearfix"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box3">
                    <ul class="list1">
                        <i class="clock1"> </i>
                        <li class="list1_right"><p>Easy Extended Returned</p></li>
                        <div class="clearfix"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box4">
                    <ul class="list1">
                        <i class="vehicle"> </i>
                        <li class="list1_right"><p>Free Shipping on order over 99 $</p></li>
                        <div class="clearfix"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box5">
                    <ul class="list1">
                        <i class="dollar"> </i>
                        <li class="list1_right"><p>Delivery Schedule Spread Cheer Time</p></li>
                        <div class="clearfix"> </div>
                    </ul>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        
        <div class="sellers_grid">
            <ul class="sellers">
                <i class="star"> </i>
                <li class="sellers_desc"><h2>Best Doctor</h2></li>
                <div class="clearfix"> </div>
            </ul>
        </div>
        <div class="grid_2">
            <?php while ($product_info=  mysqli_fetch_assoc($query_result)) { ?>
            <div class="col-md-3 span_6">
                <div class="box_inner">
                    <img src="admin/<?php echo $product_info['product_image']; ?>" alt="" height="200" width="200"/>
                    <div class="sale-box"> </div>
                    <div class="desc">
                        <h3><?php echo $product_info['product_name']; ?></h3>
                        <h4>BDT <?php echo $product_info['product_price']; ?></h4>
                        <ul class="list2">
                            <!--<li class="list2_left"><span class="m_1"><a href="#" class="link"></a></span></li>-->
                            <li class="list2_right"><span class="m_2"><a href="product_details.php?product_id=<?php echo $product_info['product_id']; ?>" class="link1"> Details </a></span></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="heart"> </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>

