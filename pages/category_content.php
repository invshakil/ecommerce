<?php
    $category_id=$_GET['category_id'];
    $query_result=$ob_app->select_product_by_category_id($category_id);
    
?>

<div class="men">
    <div class="container">
        <div class="col-md-3 sidebar">
           <div class="block block-layered-nav">
                <div class="block-title">
                    <strong><span>Shop By</span></strong>
                </div>
                <div class="block-content">
                    <dl id="narrow-by-list">
                        
                        
                        
                    </dl>

                </div>
            </div>
			
            <div class="block block-cart">
                <div class="block-title">
                    <strong><span>Carrello</span></strong>
                </div>
                <div class="block-content">
                    
                </div>
            </div>
            <div class="block block-list block-compare">
                <div class="block-title">
                    
                </div>
                <div class="block-content">
                    
                </div>
            </div>
        </div>
		
        <div class="col-md-9">
            <div class="mens-toolbar">
                <div class="sort">
                    <div class="sort-by">
                        
                        
                    </div>
                </div>
                <div class="pager">   
                    <div class="limiter visible-desktop">
                        
                        
                    </div>
                    <ul class="dc_pagination dc_paginationA dc_paginationA06">
                        <li><a href="#" class="previous">Pages</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
			
			
			
			
			
		<div class="grid_2">
				<?php while ($product_info=  mysqli_fetch_assoc($query_result)) { ?>
            <div class="col-md-3 span_6">
                <div class="box_inner">
                    <img src="admin/<?php echo $product_info['product_image']; ?>" alt="" height="200" width="200"/>
                    <div class="sale-box"> </div>
                    <div class="desc">
                        <h3><?php echo $product_info['product_name']; ?></h3>
                        <h4>Visit BDT <?php echo $product_info['product_price']; ?></h4>
                        <ul class="list2">
                            <!--<li class="list2_left"><span class="m_1"><a href="#" class="link"></a></span></li>-->
                            <li class="list2_right"><span class="m_2"><a href="product_details.php?product_id=<?php echo $product_info['product_id']; ?>" class="link11"> Details </a></span></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <div class="heart"> </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="clearfix"> </div>
        </div>
			
			
			
            <div class="span_2">
                <?php while ($category_product=  mysqli_fetch_assoc($query_result)) { ?>
                <div class="col_1_of_single1 span_1_of_single1">
                    <a href="single.html">
                        <img src="admin/<?php echo $category_product['product_image']; ?>" height="200" width="200" alt=""/>
                        <h3><?php echo $category_product['product_name']; ?></h3>
                        <h4><?php echo $category_product['product_price']; ?></h4>
						
                    </a>  
                </div> 
                <?php } ?>
                <div class="clearfix"></div>
            </div>
			
			
			
			
        </div>
    </div>
</div>